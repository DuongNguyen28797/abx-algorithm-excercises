
//https://leetcode.com/problems/palindrome-number/submissions/
var isPalindrome = function(x) {
    return x.toString() == x.toString().split('').reverse().join('');
};