 //https://leetcode.com/problems/power-of-two/submissions/
var isPowerOfTwo = function(n) {
  var tmp = ~~(Math.log(n) / Math.log(2));
  return n === (1 << tmp);
};