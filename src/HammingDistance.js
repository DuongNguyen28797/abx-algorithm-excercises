//https://leetcode.com/problems/hamming-distance/
var hammingDistance = function(x, y) {
  let ans = 0;
  while (x || y) {
    ans += (x & 1) ^ (y & 1);
    x >>= 1;
    y >>= 1;
  }
  return ans;
};